package br.com.omni.mesaefetive.starter.config;

import br.com.omni.mesaefetive.starter.service.TestService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TestAutoConfiguration {

    @Bean
    public TestService getRestTemplate() {
        return new TestService();
    }

}
