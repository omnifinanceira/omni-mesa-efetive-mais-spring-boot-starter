#!/usr/bin/env groovy

@Library('omni-infra-java-jenkins-library')_

import groovy.transform.Field
import br.com.omni.pipeline.Pipeline
import br.com.omni.pipeline.PipeFactory
import br.com.omni.pipeline.JavaRuntime
import static br.com.omni.pipeline.PipeType.JAVA_NEXUS_PRD
import static br.com.omni.pipeline.JavaRuntime.OPENJDK_11

@Field def appName = 'omni-mesa-efetive-mais-spring-boot-starter'
@Field def JavaRuntime appRuntime = OPENJDK_11

@Field def gitUrl = 'https://bitbucket.org/omnifinanceira/omni-mesa-efetive-mais-spring-boot-starter.git'

Pipeline pipe = PipeFactory.create(JAVA_NEXUS_PRD, this)
pipe.deploy()
